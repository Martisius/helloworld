package com.example.lukasmartisius.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button btnClickMe;
    TextView textCount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnClickMe = findViewById(R.id.buttonCounter);
        textCount = findViewById(R.id.counterField);
        textCount.setText("0");

        btnClickMe.setOnClickListener(MainActivity.this);
    }

    @Override
    public void onClick(View v) {
        int newInt = Integer.parseInt(textCount.getText().toString())+1;
        textCount.setText(Integer.toString(newInt));
    }
}
